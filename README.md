# Dockerfile for building the win32 version of the [LabVIEW binding for Tango](https://gitlab.com/tango-controls/labview-binding)

To build:
```
docker build -t <tag name> .
```

To extract output:
```
md dist
docker run -it --rm -v %cd%/dist:C:/dist <tag name>
```
